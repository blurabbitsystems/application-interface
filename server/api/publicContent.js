const publicContentServer = require('../services/publicContentServer');
const _ = require('lodash');

async function searchArticles(params) {
  const searchParams = _.pick(params, ['query', 'offset', 'pageSize', 'publishers']);
  return await publicContentServer.fetchArticles(searchParams);
};

async function getPublishers() {
  return await publicContentServer.fetchPublishers();
}

module.exports = {
  searchArticles,
  getPublishers,
};
