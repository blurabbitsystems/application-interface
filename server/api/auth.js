const jwt = require('jsonwebtoken');
const passport = require('passport');
const { Strategy: JwtStrategy } = require('passport-jwt');

const jwtStrategyConfig = {
  jwtFromRequest: req => req.cookies.jwt_token,
  secretOrKey: 'somesecretkey',
};

passport.use(new JwtStrategy(jwtStrategyConfig, async (decrypted_jwt, done) => {
  const user = { guid: decrypted_jwt.sub };
  done(null, user);
}));

function signToken(user) {
  const now = new Date();
  const config = {
    sub: user.guid,
    iat: now.getTime(),
    exp: new Date().setDate(now.getDate() + 1),
  };

  return jwt.sign(config, 'somesecretkey');
}

module.exports = {
  requireLogin: passport.authenticate('jwt', { session: false }),
  signToken,
};
