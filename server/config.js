class Config {
  static USER_MANAGER = 'http://user-manager:8000';
  static PUBLIC_CONTENT_SERVER = 'http://public-content-server:8000';
}

module.exports = Config;
