const express = require('express');
const router = express.Router();
const publicContentAPI = require('../api/publicContent');

router.get('/', (req, res) => {
  res.json({ msg: 'hello world' });
});

router.get('/articles/search', async (req, res) => {
  try {
    const articles = await publicContentAPI.searchArticles(req.query);
    return res.status(200).json(articles);
  } catch (err) {
    return res.status(400).json({'msg': err.message});
  }
});

router.get('/publishers', async (req, res) => {
  try {
    const publishers = await publicContentAPI.getPublishers();
    return res.status(200).json(publishers);
  } catch (err) {
    return res.status(400).json({'msg': err.message});
  }
});

module.exports = router;
