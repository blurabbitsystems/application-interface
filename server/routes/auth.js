const express = require('express');
const router = express.Router();
const auth = require('../api/auth');
const userManager = require('../services/userManager');

router.post('/signin', async (req, res) => {
  try {
    const user = await userManager.fetchUser(req.body.username, req.body.password);
    const token = auth.signToken(user);

    res
      .cookie('jwt_token', token, { path: '/api', maxAge: 360000000 })
      .status(200)
      .json(user);
  } catch(err) {
    res
      .status(404)
      .json({"msg": "username/password mismatch"});
  }
});

router.post('/signup', async (req, res) => {
  try {  
    const user = await userManager.createUser(req.body.username, req.body.password);
    const token = auth.signToken(user);

    res
    .cookie('jwt_token', token, { path: '/api', maxAge: 360000000 })
      .status(201)
      .json(user);
  } catch (err) {
    res
      .status(400)
      .json({"msg": "failed to create user"});
  }
});

router.post('/signout', auth.requireLogin, (req, res) => {
  res
    .cookie('jwt_token', '', { path: '/api' })
    .status(204)
    .json({});
});

router.get('/user', auth.requireLogin, async (req, res) => {
  try {  
    const user = await userManager.fetchUserByGuid(req.user.guid);

    res
      .status(200)
      .json(user);
  } catch (err) {
    res
      .status(404)
      .json({"msg": "no user found"});
  }
});

module.exports = router;
