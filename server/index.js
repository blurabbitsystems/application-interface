const express = require('express');
const bodyParser = require('body-parser');
const cookieParser = require('cookie-parser')
const cors = require('cors');

const app = express();

// Constants
const HOST = '0.0.0.0';
const PORT = 8000;

// Middleware
app.use(bodyParser.json());
app.use(cors());
app.use(cookieParser());

// Routes
const publicContent = require('./routes/publicContent');
app.use('/api/public-content', publicContent);

const auth = require('./routes/auth');
app.use('/api/auth', auth);

// Server
app.listen(PORT, HOST, () => {
  console.log('Express server initiated.');
});
