const http = require('../utils/http');
const Config = require('../config');

function fetchUser(username, password) {
  const url = Config.USER_MANAGER + '/api/users/checkpassword/';
  return http.post(url, { username, password }).then(resp => resp.data);
}

function createUser(username, password) {
  const url = Config.USER_MANAGER + '/api/users/';
  return http.post(url, { username, password }).then(resp => resp.data);
}

function fetchUserByGuid(guid) {
  const url = Config.USER_MANAGER + '/api/users/' + guid;
  return http.get(url).then(resp => resp.data);
}

module.exports = {
  fetchUser,
  createUser,
  fetchUserByGuid,
};
