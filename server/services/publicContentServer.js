const http = require('../utils/http');
const Config = require('../config');

const articleEndpoint = Config.PUBLIC_CONTENT_SERVER + '/api/content/articles/';
const publisherEndpoint = Config.PUBLIC_CONTENT_SERVER + '/api/publishers/';

function fetchArticles({ query, publishers, offset, pageSize }) {
  const params = {
    q: query,
    offset,
    page_size: pageSize,
    publishers,
  };

  return http.get(articleEndpoint, { params }).then(resp => resp.data);
}

function fetchPublishers() {
  return http.get(publisherEndpoint).then(resp => resp.data);
}

module.exports = {
  fetchArticles,
  fetchPublishers,
};
