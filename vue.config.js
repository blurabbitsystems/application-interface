const path = require("path");

module.exports = {
  chainWebpack: config => {
    config
      .entry("app")
      .clear()
      .add("./client/main.js")
      .end();
    config.resolve.alias
      .set("@", path.join(__dirname, "./client"))
  },
  devServer: {
    proxy: {
      '/api': {
        target: 'http://0.0.0.0:8000'
      }
    }
  },
  css: {
    loaderOptions: {
      sass: {
        prependData: `@import "@/variables.scss";`
      }
    }
  },
};
