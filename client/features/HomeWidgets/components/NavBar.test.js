import { assert } from 'chai';
import { shallowMount } from '@vue/test-utils';
import NavBar from './NavBar.vue';
import sinon from 'sinon';

function render(props, $router, $route, $store) {
  return shallowMount(NavBar, {
    propsData: props,
    stubs: [
      'b-img',
    ],
    mocks: {
      $router,
      $route,
      $store,
    },
  });
}

describe('NavBar', () => {
  let wrapper, sandbox, fakeRoute, fakeRouter, fakeStore;

  before(() => {
    sandbox = sinon.createSandbox();
    fakeRoute = { name: NavBar.TABS[1].routeName };
    fakeRouter = {
      push: sandbox.stub(),
    };
    fakeStore = {
      dispatch: sandbox.stub(),
    };

    wrapper = render({}, fakeRouter, fakeRoute, fakeStore);
  });

  after(() => {
    wrapper && wrapper.destroy();
  });

  it('renders the dom with the expected structure.', () => {
    assert.lengthOf(wrapper.findAll('div'), 7);

    const navbar = wrapper.find('.navbar');
    assert.exists(navbar);
    assert.isTrue(navbar.is('div'));

    const options = navbar.find('.options');
    assert.exists(options);
    assert.isTrue(options.is('div'));

    const avatar = navbar.find('.avatar');
    assert.exists(avatar);
    assert.isTrue(avatar.is('div'));

    const thumbnail = avatar.find('.thumbnail');
    assert.exists(thumbnail);
  });

  it('displays the tabs properly.', () => {
    const tabDoms = wrapper.findAll('.options > div');
    NavBar.TABS.forEach(({ label }, idx) => {
      assert.equal(tabDoms.at(idx).element.innerHTML, label);
    });
  });

  it('highlights the selected tab.', () => {
    const tabDoms = wrapper.findAll('.options > div');
    NavBar.TABS.forEach(({ routeName }, idx) => {
      const tab = tabDoms.at(idx);
      if (routeName !== fakeRoute.name) {
        assert.isEmpty(tab.classes());
      } else {
        assert.deepEqual(tab.classes(), ['active']);
      }
    });
  });

  it('displays the avatar properly.', () => {
    const thumbnail = wrapper.find('.avatar > .thumbnail');
    assert.include(thumbnail.attributes(), {
      rounded: 'circle',
    });
  });

  it('triggers tab-switch when clicked on a tab.', async () => {
    const tabDoms = wrapper.findAll('.options > div');
    await tabDoms.at(0).trigger('click');
    assert.isTrue(fakeRouter.push.calledOnceWith('home'));
  });

  it('triggers logout when clicked on the avatar.', async () => {
    fakeRouter.push.reset();

    const avatar = wrapper.find('.avatar');
    await avatar.trigger('click');
    assert.isTrue(fakeStore.dispatch.calledOnceWith('logout'));
    assert.isTrue(fakeRouter.push.calledOnceWith('signin'));
  });
});
