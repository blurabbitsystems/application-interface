import http from '@/utils/http';

const publicContentEP = '/api/public-content';

function getPublishers() {
  const url = publicContentEP + '/publishers';
  return http.get(url).then(resp => resp.data);
}

function getContent(params) {
  switch (params.contentType) {
    case 'article':
      return _getArticles(_.omit(params, ['contentType']));
    case 'image':
      return _getImages(_.omit(params, ['contentType']));
    default:
      throw 'Content type is invalid.';
  }
}

function _getArticles(params) {
  const url = publicContentEP + '/articles/search';
  params = {
    q: params.q,
    publishers: (params.publishers || []).join(','),
    offset: params.offset,
    pageSize: params.pageSize,
  };

  return http.get(url, { params }).then(resp => resp.data);
}

function _getImages() {
  return new Promise(resolve => {
    resolve([]);
  });
}

export {
  getPublishers,
  getContent,
};
