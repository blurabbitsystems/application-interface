import { assert } from 'chai';
import nock from 'nock';
import * as PublicContent from './PublicContent';


describe('PublicContent', () => {
  let faker;

  before(() => {
    faker = nock('http://localhost');
  });

  after(() => {
    nock.cleanAll();
  });

  describe('.getPublishers', () => {
    it('works.', async () => {
      const scope = faker.get('/api/public-content/publishers').reply(200, {});
      assert.deepEqual(await PublicContent.getPublishers(), {});
      assert.isTrue(scope.isDone());
    });
  });

  describe('.getContent', () => {
    it('works for article content-type.', async () => {
      const params = { q: 'lol', contentType: 'article', publishers: ['a', 'b'], offset: 10, pageSize: 20 };
      const qstring = 'q=lol&publishers=a,b&offset=10&pageSize=20';
      const scope = faker.get('/api/public-content/articles/search?' + qstring).reply(200, {});
      assert.deepEqual(await PublicContent.getContent(params), {});
      assert.isTrue(scope.isDone());
    });

    it('throws error for invalid content-type.', () => {
      const params = { contentType: 'invalid' };
      assert.throws(() => PublicContent.getContent(params), 'Content type is invalid.');
    });
  });
});
