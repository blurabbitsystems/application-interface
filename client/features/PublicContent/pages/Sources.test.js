import { assert } from 'chai';
import { shallowMount, createLocalVue } from '@vue/test-utils';
import VueLodash from 'vue-lodash';
import sinon from 'sinon';
import infiniteScroll from 'vue-infinite-scroll';
import BootstrapVue from 'bootstrap-vue'
import Sources from './Sources.vue';
import * as PublicContent from '../services/PublicContent';
import _ from 'lodash';


function render(props) {
  const localVue = createLocalVue();
  localVue.use(VueLodash, { lodash: _ });
  localVue.use(infiniteScroll);
  localVue.use(BootstrapVue);

  return shallowMount(Sources, {
    localVue,
    propData: props,
  });
}

describe('Sources', () => {
  let wrapper, sandbox, getContent, getPublishers, fakePublishers;

  before(async () => {
    sandbox = sinon.createSandbox();
    fakePublishers = [
      { title: 'Al Jazeera', guid: 'e23a1cdf' },
      { title: 'BBC News', guid: 'a2def1c3' },
    ];

    getPublishers = sandbox.stub(PublicContent, 'getPublishers');
    getPublishers.resolves(fakePublishers);
    getContent = sandbox.stub(PublicContent, 'getContent');
    getContent.resolves({ returned: 0, results: [] });

    wrapper = await render({});
  });

  after(() => {
    sandbox.restore();
    wrapper && wrapper.destroy();
  });

  it('renders the dom with the expected structure.', () => {
    const sources = wrapper.find('.sources');
    assert.isTrue(sources.is('div'));

    const title = sources.find('.title');
    assert.isTrue(title.is('div'));

    const filterPanelContainer = sources.find('.filter-panel-container');
    assert.isTrue(filterPanelContainer.is('div'));

    const filterPanel = filterPanelContainer.find('filter-panel-stub');
    assert.isTrue(filterPanel.exists());

    const publicContent = sources.find('.public-content');
    assert.isTrue(publicContent.is('div'));
  });

  it('renders the filter panel properly.', () => {
    const filterPanel = wrapper.find('filter-panel-stub');
    assert.deepEqual(filterPanel.vm.filters, [
      { name: 'contentType', label: 'Content Type', type: 'dropdown', prepend: true, options: [{label: 'Article', value: 'article'}, {label: 'Image', value: 'image'}] },
      { name: 'q', label: 'Search', type: 'textinput', prepend: true },
      { name: 'publishers', label: 'Publishers', type: 'checkbox', options: [{ label: 'Al Jazeera', name: 'e23a1cdf' }, { label: 'BBC News', name: 'a2def1c3' }] },
    ]);
  });

  it('fetches the publishers.', () => {
    assert.isTrue(getPublishers.calledOnce);
  });

  it('fetches the content.', () => {
    assert.isTrue(getContent.calledOnceWith({
      q: '',
      contentType: 'article',
      publishers: [],
      offset: 0,
      pageSize: 10,
    }));
  });

  it('displays an empty-content message if no content was found.', () => {
    const noContent = wrapper.find('.no-content');
    assert.equal(noContent.text(), 'No content found!');
  });

  it('refreshes content if detects any change in filter panel.', () => {
    getContent.reset();

    const filterPanel = wrapper.find('filter-panel-stub');
    filterPanel.vm.$emit('change', { contentType: 'image' });
    assert.isTrue(getContent.calledOnceWith({
      q: '',
      contentType: 'image',
      publishers: [],
      offset: 0,
      pageSize: 10,
    }));
  });

  describe('case: article content-type', () => {
    let wrapper_nested, fakeContent;

    before(() => {
      fakeContent = [
        { guid: '34fc2ac' },
        { guid: '4f3cc2a' },
        { guid: '24fa3cc' },
      ];
      getContent.reset();
      getContent.resolves({ returned: 3, results: fakeContent });
      wrapper_nested = render({});
    });

    after(() => {
      wrapper_nested && wrapper_nested.destroy();
    });

    it('renders the article cards.', () => {
      const articleCards = wrapper_nested.findAll('.article-card');
      assert.lengthOf(articleCards, 3);
      fakeContent.forEach((content, idx) => {
        assert.deepEqual(articleCards.at(idx).vm.article, content);
      });
    });

    it('renders the article previewer modal.', () => {
      const articlePreviewer = wrapper_nested.find('article-previewer-stub');
      assert.isTrue(articlePreviewer.exists());
    });

    it('opens the article previewer modal when clicked on an article card.', async () => {
      const showModal = sandbox.spy(wrapper_nested.vm.$bvModal, 'show');
      const articleCard = wrapper_nested.find('.article-card');
      await articleCard.vm.$emit('click');
      assert.isTrue(showModal.calledOnceWith('article-previewer'));
    });
  });
});
