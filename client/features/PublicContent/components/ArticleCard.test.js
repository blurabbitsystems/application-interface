import { assert } from 'chai';
import { shallowMount } from '@vue/test-utils';
import ArticleCard from './ArticleCard.vue';
import _ from 'lodash';


describe('ArticleCard', () => {
  let wrapper, fakeArticle, fakeFilters;

  before(() => {
    fakeArticle = {
      title: 'Greetings from Moscow, Professor!!',
      description: '<p>Saul Berenson gets a memo from Carrie, from all the way to Moscow.</p>',
      publisher: {
        guid: '79baab13715344d486dc270714e6b262',
        title: 'The Rising Star',
      },
      published_at: '2020-05-21T18:20:38Z',
      author: 'Carrie Mathison',
    };

    fakeFilters = {
      formatDate: _.constant('21 Feb, 2020'),
    };

    wrapper = shallowMount(ArticleCard, {
      propsData: {
        article: fakeArticle,
      },
      filters: fakeFilters,
    });
  });

  after(() => {
    wrapper && wrapper.destroy();
  });

  it('renders the dom with the expected structure.', () => {
    assert.lengthOf(wrapper.findAll('div'), 3);
    assert.lengthOf(wrapper.findAll('span'), 2);

    const articleCard = wrapper.find('.article-card');
    assert.exists(articleCard);
    assert.isTrue(articleCard.is('div'));

    const title = articleCard.find('.title');
    assert.exists(title);
    assert.isTrue(title.is('div'));

    const body = articleCard.find('.body');
    assert.exists(body);
    assert.isTrue(body.is('formatted-description-stub'));

    const footer = articleCard.find('.footer');
    assert.exists(footer);
    assert.isTrue(footer.is('div'));

    const publisherTitle = footer.find('.publisher-title');
    assert.exists(publisherTitle);
    assert.isTrue(publisherTitle.is('span'));

    const publishDate = footer.find('.publish-date');
    assert.exists(publishDate);
    assert.isTrue(publishDate.is('span'));
  });

  it('displays the article title as passed.', () => {
    const title = wrapper.find('.title').text();
    assert.equal(title, fakeArticle.title);
  });

  it('renders the formatted description.', () => {
    const body = wrapper.find('.body');
    assert.equal(body.vm.description, fakeArticle.description);
    assert.isTrue(body.vm.truncate);
  });

  it('displays the publisher title as passed.', () => {
    const publisherTitle = wrapper.find('.publisher-title').text();
    assert.equal(publisherTitle, `Publisher: ${fakeArticle.publisher.title}`);
  });

  it('formats the published date properly.', () => {
    const publishDate = wrapper.find('.publish-date').text();
    assert.equal(publishDate, `Published at: ${fakeFilters.formatDate(fakeArticle.published_at)}`);
  });

  it('listens to click events on the component.', () => {
    wrapper.trigger('click');
    assert.lengthOf(wrapper.emitted('click'), 1);
  });
});
