import { assert } from 'chai';
import { shallowMount, createLocalVue } from '@vue/test-utils';
import ArticlePreviewer from './ArticlePreviewer.vue';
import VueLodash from 'vue-lodash';
import sinon from 'sinon';
import _ from 'lodash';

function render(props, filters, close) {
  const localVue = createLocalVue();
  localVue.use(VueLodash, { lodash: _ });

  const bModalStub = localVue.component('b-modal', {
    methods: { close },
    template: `
      <div class="b-modal-stub">
        <slot name="modal-header"></slot>
        <slot></slot>
        <slot name="modal-footer" :close="close"></slot>
      </div>
    `,
  });

  return shallowMount(ArticlePreviewer, {
    stubs: {
      'b-modal': bModalStub,
    },
    localVue,
    propsData: props,
    filters,
  });
}

describe('ArticlePreviewer', () => {
  let wrapper, sandbox, fakeArticle, fakeFilters, fakeClose;

  before(() => {
    sandbox = sinon.createSandbox();
    fakeFilters = {
      formatDate: _.constant('21 Feb, 2020'),
    };
    fakeClose = sandbox.stub();
    fakeArticle = {
      title: 'Greetings from Moscow, Professor!!',
      description: '<p>Saul Berenson gets a memo from Carrie, from all the way to Moscow.</p>',
      publisher: {
        guid: '79baab13715344d486dc270714e6b262',
        title: 'The Rising Star',
      },
      published_at: '2020-05-21T18:20:38Z',
      author: 'Carrie Mathison',
    };
    wrapper = render({
      article: fakeArticle,
      origin: 'Fake Origin',
    }, fakeFilters, fakeClose);
  });

  after(() => {
    wrapper && wrapper.destroy();
  });

  it('renders the dom with the expected structure.', () => {
    const bModal = wrapper.find('.b-modal-stub');
    assert.equal(bModal.attributes('id'), 'article-previewer');

    const header = bModal.find('.header');
    assert.isTrue(header.is('div'));

    const origin = header.find('.origin');
    assert.isTrue(origin.is('div'));

    const title = header.find('.title');
    assert.isTrue(title.is('div'));

    const publisherInfo = header.find('.publisher-info');
    assert.isTrue(publisherInfo.is('div'));

    const publisherTitle = publisherInfo.find('.publisher-title');
    assert.isTrue(publisherTitle.is('span'));

    const publishDate = publisherInfo.find('.publish-date');
    assert.isTrue(publishDate.is('span'));

    const body = bModal.find('.body');
    assert.isTrue(body.is('div'));

    const author = body.find('.author');
    assert.isTrue(author.is('div'));

    const description = body.find('.description');
    assert.isTrue(description.is('formatted-description-stub'));

    const footer = bModal.find('.footer');
    assert.isTrue(footer.is('div'));

    const publishBtn = footer.find('.btn[label="Publish"]');
    assert.isTrue(publishBtn.is('c-button-stub'));

    const closeBtn = footer.find('.btn[label="Close"]');
    assert.isTrue(closeBtn.is('c-button-stub'));
  });

  it('displays the content origin.', () => {
    const origin = wrapper.find('.origin').text();
    assert.equal(origin, 'Fake Origin');
  });

  it('displays the article title.', () => {
    const title = wrapper.find('.title').text();
    assert.equal(title, fakeArticle.title);
  });

  it('displays the article author.', () => {
    const author = wrapper.find('.author').text();
    assert.equal(author, `Author: ${fakeArticle.author}`);
  });

  it('renders the formatted article description.', () => {
    const body = wrapper.find('.description');
    assert.equal(body.vm.description, fakeArticle.description);
    assert.isFalse(body.vm.truncate);
  });

  it('displays the publisher title.', () => {
    const publisherTitle = wrapper.find('.publisher-title').text();
    assert.equal(publisherTitle, `Publisher: ${fakeArticle.publisher.title}`);
  });

  it('formats the published date properly.', () => {
    const publishDate = wrapper.find('.publish-date').text();
    assert.equal(publishDate, `Published at: ${fakeFilters.formatDate(fakeArticle.published_at)}`);
  });

  it('closes the modal on close button click.', async () => {
    const closeBtn = wrapper.find('.btn[label="Close"]');
    await closeBtn.vm.$emit('click');
    assert.isTrue(fakeClose.called);
  });
});
