import http from '@/utils/http';

const authEP = '/api/auth';

function getUser() {
  return http.get(`${authEP}/user`).then(resp => resp.data);
}

function requestLogin({ username, password }) {
  return http.post(`${authEP}/signin`, { username, password }).then(resp => resp.data);
}

function requestRegistration({ username, password }) {
  return http.post(`${authEP}/signup`, { username, password }).then(resp => resp.data);
}

function requestLogout() {
  return http.post(`${authEP}/signout`).then(resp => resp.data);
}

export {
  getUser,
  requestLogin,
  requestRegistration,
  requestLogout,
};
