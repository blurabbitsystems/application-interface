import nock from 'nock';
import { assert } from 'chai';
import * as Auth from './Auth';


describe('Auth', () => {
  let faker;

  before(() => {
    faker = nock('http://localhost');
  });

  after(() => {
    nock.cleanAll();
  });

  describe('.getUser', () => {
    it('works.', async () => {
      const scope = faker.get('/api/auth/user').reply(200, {});
      assert.deepEqual(await Auth.getUser(), {});
      assert.isTrue(scope.isDone());
    });
  });

  describe('.requestLogin', () => {
    it('works.', async () => {
      const scope = faker.post('/api/auth/signin', {username: 'user', password: 'pass'}).reply(200, {});
      assert.deepEqual(await Auth.requestLogin({username: 'user', password: 'pass'}), {});
      assert.isTrue(scope.isDone());
    });
  });

  describe('.requestRegistration', () => {
    it('works.', async () => {
      const scope = faker.post('/api/auth/signup', {username: 'user', password: 'pass'}).reply(200, {});
      assert.deepEqual(await Auth.requestRegistration({username: 'user', password: 'pass'}), {});
      assert.isTrue(scope.isDone());
    });
  });

  describe('.requestLogout', () => {
    it('works.', async () => {
      const scope = faker.post('/api/auth/signout').reply(200, {});
      assert.deepEqual(await Auth.requestLogout(), {});
      assert.isTrue(scope.isDone());
    });
  });
});
