import Vue from 'vue'
import App from './App.vue'
import './registerServiceWorker'
import router from './router'
import store from './store'
import BootstrapVue from 'bootstrap-vue'
import VueLodash from 'vue-lodash'
import lodash from 'lodash'
import infiniteScroll from 'vue-infinite-scroll'
import '@fortawesome/fontawesome-free/css/all.css'
import '@fortawesome/fontawesome-free/js/all.js'

Vue.config.productionTip = false
Vue.use(BootstrapVue)
Vue.use(VueLodash, { lodash })
Vue.use(infiniteScroll)
Vue.filter('formatDate', value => {
  const datetime = new Date(value);
  return `${datetime.toLocaleTimeString()}, ${datetime.toDateString()}`;
})

async function initiateApp() {
  await store.dispatch('reloadUser');
  new Vue({
    router,
    store,
    render: h => h(App)
  }).$mount('#app');
}

initiateApp();
