import { assert } from 'chai';
import { shallowMount } from '@vue/test-utils';
import Dropdown from './Dropdown.vue';
import _ from 'lodash';

function render(props) {
  return shallowMount(Dropdown, {
    propsData: props,
  });
}

describe('Dropdown', () => {
  let wrapper, options;

  before(() => {
    options = [
      { label: 'Chocolate', value: 'chocolate' },
      { label: 'Ice Cream', value: 'ice-cream' },
      { label: 'Custard', value: 'custard' },
    ];

    wrapper = render({
      label: 'Select One!!',
      options,
    });
  });

  after(() => {
    wrapper && wrapper.destroy();
  });

  it('renders the dom with the expected structure.', () => {
    const dropdown = wrapper.find('.dropdown');
    assert.exists(dropdown);
    assert.isTrue(dropdown.is('div'));

    const select = dropdown.find('select');
    assert.exists(select);

    const optgroup = select.find('optgroup');
    assert.exists(optgroup);

    const optionDoms = optgroup.findAll('option');
    assert.lengthOf(optionDoms, 3);
  });

  it('displays the label properly.', () => {
    const label = wrapper.find('optgroup').attributes('label');
    assert.equal(label, 'Select One!!');
  });

  it('renders the options properly.', () => {
    const optionDoms = wrapper.findAll('option');
    options.forEach(({ label, value }, idx) => {
      let dom = optionDoms.at(idx);
      assert.equal(dom.text(), label);
      assert.equal(dom.element.value, value);
    });
  });

  it('listens to select events on the component.', async () => {
    const select = wrapper.find('select');
    await wrapper.setData({ selected: 'custard' });
    await select.trigger('change');
    assert.deepEqual(wrapper.emitted('select'), [['custard']]);
  });

  describe('case: initial value', () => {
    let wrapper_nested;

    after(() => {
      wrapper_nested && wrapper_nested.destroy();
    });

    it('respects the initial value.', () => {
      wrapper_nested = render({ label: 'Select', options, value: 'ice-cream' });

      const select = wrapper_nested.find('select');
      assert.equal(select.element.value, 'ice-cream');
    });

    it('falls back to the first option, if initial value is not an option', () => {
      wrapper_nested = render({ label: 'Select', options, value: 'pudding' });

      const select = wrapper_nested.find('select');
      assert.equal(select.element.value, 'chocolate');
    });
  });

  describe('case: empty options list', () => {
    let wrapper_nested;

    before(() => {
      wrapper_nested = render({
        label: 'No options :-(',
        options: [],
      });
    });

    after(() => {
      wrapper_nested && wrapper_nested.destroy();
    });

    it('maintains an empty value as present seletion.', () => {
      const select = wrapper_nested.find('select');
      assert.equal(select.element.value, '');
    });

    it('does not render any option.', () => {
      const optionDoms = wrapper_nested.findAll('option');
      assert.lengthOf(optionDoms, 0);
    });
  })
});
