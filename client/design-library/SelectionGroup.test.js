import { assert } from 'chai';
import { shallowMount } from '@vue/test-utils';
import SelectionGroup from './SelectionGroup.vue';

function render(props) {
  return shallowMount(SelectionGroup, {
    propsData: props,
  });
}

describe('SelectionGroup', () => {
  let wrapper, options;

  before(() => {
    options = [
      { name: 'pizza', label: 'Pizza' },
      { name: 'pasta', label: 'Pasta' },
    ];
    wrapper = render({
      label: 'Selections:',
      options,
    });
  });

  after(() => {
    wrapper && wrapper.destroy();
  });

  it('renders the dom with the expected structure.', () => {
    const selectionGroup = wrapper.find('.selection-group');
    assert.exists(selectionGroup);
    assert.isTrue(selectionGroup.is('div'));

    const label = selectionGroup.find('.label');
    assert.exists(label);
    assert.isTrue(label.is('div'));

    const checkboxes = selectionGroup.findAll('.checkbox');
    assert.lengthOf(checkboxes, 2);
  });

  it('displays the label properly.', () => {
    const label = wrapper.find('.label').text();
    assert.equal(label, 'Selections:');
  });

  it('renders the options properly.', () => {
    const checkboxes = wrapper.findAll('.checkbox');
    options.forEach(({ label }, idx) => {
      const dom = checkboxes.at(idx);
      assert.equal(dom.attributes('label'), label);
      assert.equal(dom.vm.value, false);
    });
  });

  it('listens to change events of the component.', async () => {
    const checkbox = wrapper.find('.checkbox');   // Pizza
    await checkbox.vm.$emit('change', true);
    assert.deepEqual(wrapper.emitted('change'), [[['pizza']]]);
  });

  describe('case: initial value', () => {
    let wrapper_nested;

    before(() => {
      wrapper_nested = render({
        label: 'Selections',
        options,
        value: ['pasta', 'cheeze'],
      });
    });

    after(() => {
      wrapper_nested && wrapper_nested.destroy();
    });

    it('respects the initial value.', () => {
      const checkbox = wrapper_nested.findAll('.checkbox').at(1);  // Pasta
      assert.equal(checkbox.vm.value, true);
    });

    it('trims out any invalid initial value.', () => {
      assert.deepEqual(wrapper_nested.vm.states, {'pizza': false, 'pasta': true});
    });
  });
});
