import { assert } from 'chai';
import { shallowMount } from '@vue/test-utils';
import Checkbox from './Checkbox.vue';

function render(props) {
  return shallowMount(Checkbox, {
    propsData: props,
  });
}

describe('Checkbox', () => {
  let wrapper;

  before(() => {
    wrapper = render({
      label: 'Tick Me!!',
    });
  });

  after(() => {
    wrapper && wrapper.destroy();
  });

  it('renders the dom with the expected structure.', () => {
    assert.isFalse(wrapper.vm.state);

    const checkbox = wrapper.find('.checkbox');
    assert.exists(checkbox);
    assert.isTrue(checkbox.is('label'));

    const input = checkbox.find('input');
    assert.exists(input);
    assert.equal(input.attributes('type'), 'checkbox');

    const checkmark = checkbox.find('.checkmark');
    assert.exists(checkmark);
    assert.isTrue(checkmark.is('span'));
  });

  it('displays the label properly.', () => {
    const label = wrapper.text();
    assert.equal(label, 'Tick Me!!');
  });

  it('listens to change events on the component.', async () => {
    const input = wrapper.find('input');
    await input.trigger('click');
    assert.deepEqual(wrapper.emitted('change'), [[true]]);
    assert.isTrue(wrapper.vm.state);
  });

  describe('case: initial value', () => {
    let wrapper_nested;

    before(() => {
      wrapper_nested = render({
        label: 'Tick Me!!',
        value: true,
      });
    });

    after(() => {
      wrapper_nested && wrapper_nested.destroy();
    });

    it('respects the initial value.', () => {
      assert.isTrue(wrapper_nested.vm.state);
    });
  });
});
