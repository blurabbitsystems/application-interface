import { assert } from 'chai';
import { shallowMount } from '@vue/test-utils';
import TextInput from './TextInput.vue';

function render(props) {
  return shallowMount(TextInput, {
    propsData: props,
  });
}

describe('TextInput', () => {
  let wrapper;

  before(() => {
    wrapper = render({
      placeholder: 'Type here!!',
    });
  });

  after(() => {
    wrapper && wrapper.destroy();
  });

  it('renders the dom with the expected structure.', () => {
    const textinput = wrapper.find('.textinput');
    assert.exists(textinput);
    assert.isTrue(textinput.is('div'));

    const input = textinput.find('input');
    assert.exists(input);
  });

  it('renders the input element with the right attributes.', () => {
    const input = wrapper.find('input');
    assert.equal(input.element.value, '');
    assert.include(input.attributes(), {
      placeholder: 'Type here!!',
      type: 'text',
    });
  });

  it('listens to input events of the component.', async () => {
    await wrapper.setProps({ value: 'some-te' });

    const input = wrapper.find('input');
    input.trigger('input');
    assert.deepEqual(wrapper.emitted('input'), [['some-te']]);
  });

  it('listens to submit events of the component.', async () => {
    await wrapper.setProps({ value: 'some-text...' });

    const input = wrapper.find('input');
    input.trigger('keypress.enter');
    assert.deepEqual(wrapper.emitted('submit'), [['some-text...']]);
  });

  describe('case: non-default types', () => {
    let wrapper_nested;

    it('respects the type prop.', () => {
      wrapper_nested = render({ type: 'password' });

      const input = wrapper_nested.find('input');
      assert.equal(input.attributes('type'), 'password');
    });

    it('fallbacks to the \'text\' type for an invalid type prop.', () => {
      wrapper_nested = render({ type: 'invalidtype' });

      const input = wrapper_nested.find('input');
      assert.equal(input.attributes('type'), 'text');
    });
  });

  describe('case: initial value', () => {
    let wrapper_nested;

    it('respects the initial value.', () => {
      wrapper_nested = render({ value: 'some-text'});

      const input = wrapper_nested.find('input');
      assert.equal(input.element.value, 'some-text');
    });
  });
});
