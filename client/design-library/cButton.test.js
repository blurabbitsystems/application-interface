import { assert } from 'chai';
import { shallowMount } from '@vue/test-utils';
import cButton from './cButton.vue';

function render(props) {
  return shallowMount(cButton, {
    propsData: props,
    stubs: [
      'b-button',
    ],
  });
}

describe('cButton', () => {
  let wrapper;

  before(() => {
    wrapper = render({
      label: 'Click Me!!',
    });
  });

  after(() => {
    wrapper && wrapper.destroy();
  });

  it('renders the dom with the expected structure.', () => {
    const button = wrapper.find('.button');
    assert.exists(button);
    assert.include(button.attributes(), {
      size: 'sm',
      variant: 'secondary-outline',
      class: 'button',
    });
  });

  it('displays the label properly.', () => {
    const label = wrapper.find('.button').text();
    assert.equal(label, 'Click Me!!');
  });

  describe('- case: primary button', () => {
    let wrapper_nested;

    before(() => {
      wrapper_nested = render({
        label: 'Click Me!!',
        primary: true,
      });
    });

    after(() => {
      wrapper_nested && wrapper_nested.destroy();
    });

    it('renders with valid css class.', () => {
      const button = wrapper_nested.find('.button');
      assert.include(button.attributes(), {
        size: 'sm',
        variant: 'secondary',
        class: 'button primary',
      }); 
    });
  });
});
