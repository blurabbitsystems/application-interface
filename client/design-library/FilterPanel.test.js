import { assert } from 'chai';
import { shallowMount, mount } from '@vue/test-utils';
import FilterPanel from './FilterPanel.vue';
import _ from 'lodash';

function render(props) {
  return shallowMount(FilterPanel, {
    propsData: props,
  });
}

describe('FilterPanel', () => {
  let wrapper, filters;

  before(() => {
    filters = [
      {
        type: 'dropdown',
        label: 'Source Type',
        name: 'source_type',
        options: [
          { label: 'Express', name: 'express' },
          { label: 'Regular', name: 'regular' },
        ],
      },
      {
        type: 'textinput',
        label: 'Query',
        name: 'q',
        prepend: true,
      },
      {
        type: 'checkbox',
        label: 'Sources',
        name: 'sources',
        options: [
          { label: 'Al Jazeera', name: 'al_jazeera' },
          { label: 'CNN', name: 'cnn' },
        ],
      },
    ];

    wrapper = render({
      filters,
    });
  });

  after(() => {
    wrapper && wrapper.destroy();
  });

  it('renders the dom with expected structure.', () => {
    assert.lengthOf(wrapper.findAll('div'), 2);

    const filterPanel = wrapper.find('.filterpanel');
    assert.exists(filterPanel);
    assert.isTrue(filterPanel.is('div'));

    const filterGroups = filterPanel.findAll('.filtergroup');
    assert.lengthOf(filterGroups, 3);

    const header = filterPanel.find('.header');
    assert.exists(header);
    assert.isTrue(header.is('div'));
  });

  it('renders the dropdown filters properly.', () => {
    const dropdownFilters = wrapper.findAll('.dropdown');
    _.filter(filters, { type: 'dropdown' }).forEach(({ label, options }, idx) => {
      const dom = dropdownFilters.at(idx);
      assert.isTrue(dom.is('dropdown-stub'));
      assert.deepEqual(dom.vm.options, options);
      assert.equal(dom.vm.label, label);
    });
  });

  it('renders the checkbox groups properly.', () => {
    const checkboxGroups = wrapper.findAll('.checkbox');
    _.filter(filters, { type: 'checkbox' }).forEach(({ label, options }, idx) => {
      const dom = checkboxGroups.at(idx);
      assert.isTrue(dom.is('selection-group-stub'));
      assert.deepEqual(dom.vm.options, options);
      assert.equal(dom.vm.label, label);
    });
  });

  it('renders the textinput filters properly.', () => {
    const textInputFilters = wrapper.findAll('.textinput');
    _.filter(filters, { type: 'textinput' }).forEach(({ label }, idx) => {
      const dom = textInputFilters.at(idx);
      assert.isTrue(dom.is('text-input-stub'));
      assert.equal(dom.vm.placeholder, label);
    });
  });

  it('places the \'prepend\' filters before the \'Filters\' title.', () => {
    assert.equal(wrapper.find('.textinput + .header').text(), 'Filters');
  });

  it('listens to dropdown change events of the component.', async () => {
    wrapper.vm.params = {};
    const dropdown = wrapper.findAll('.dropdown').at(0);
    await dropdown.vm.$emit('select', 'express');
    assert.deepEqual(_.last(wrapper.emitted('change')), [{source_type: 'express'}]);
  });

  it('listens to textinput submit events of the component.', async () => {
    wrapper.vm.params = {};
    const textInput = wrapper.findAll('.textinput').at(0);
    await textInput.vm.$emit('submit', 'some query');
    assert.deepEqual(_.last(wrapper.emitted('change')), [{q: 'some query'}]);
  });

  it('listens to selection change events of the component.', async () => {
    wrapper.vm.params = {};
    const checkboxGroup = wrapper.findAll('.checkbox').at(0);
    await checkboxGroup.vm.$emit('change', ['cnn']);
    assert.deepEqual(_.last(wrapper.emitted('change')), [{sources: ['cnn']}]);
  });

  describe('case: initial value', () => {
    let wrapper_nested;

    before(() => {
      wrapper_nested = render({
        filters,
        values: {
          source_type: 'express',
          q: 'query string',
          sources: ['cnn', 'risingbd'],
          authors: ['ross'],
        },
      });
    });

    after(() => {
      wrapper_nested && wrapper_nested.destroy();
    });

    it('respects the initial values.', () => {
      assert.deepInclude(wrapper_nested.vm.params, {
        source_type: 'express',
        q: 'query string',
        sources: ['cnn', 'risingbd'],
      });
    });

    it('trims out any invalid initial value.', () => {
      assert.notInclude(_.keys(wrapper_nested.vm.params), 'authors');
    });
  });
});
