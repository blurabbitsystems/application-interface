import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '@/features/HomeWidgets/pages/Home.vue'
import Assets from '../views/Assets.vue'
import Sources from '@/features/PublicContent/pages/Sources.vue'
import Publications from '../views/Publications.vue'
import Auth from '@/features/Authentication/pages/Auth.vue';
import NotFound from '../views/NotFound.vue';
import store from '../store';

Vue.use(VueRouter)

const routes = [
  {
    path: '/home',
    alias: '/',
    name: 'home',
    component: Home,
  },
  {
    path: '/sources',
    name: 'sources',
    component: Sources,
  },
  {
    path: '/assets',
    name: 'assets',
    component: Assets,
  },
  {
    path: '/publications',
    name: 'publications',
    component: Publications,
  },
  {
    path: '/signin',
    name: 'signin',
    component: Auth,
    meta: {
      noAuth: true,
    },
  },
  {
    path: '*',
    name: '404',
    component: NotFound,
  },
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})


router.beforeEach((dstRoute, srcRoute, next) => {
  if (dstRoute.matched.some(record => !record.meta.noAuth)) {
    return store.getters.signedIn ? next() : next('/signin');
  }

  return store.getters.signedIn ? next('/home') : next();
});

export default router
