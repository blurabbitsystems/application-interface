import Vue from 'vue'
import Vuex from 'vuex'
import _ from 'lodash'
import { getUser, requestLogin, requestRegistration, requestLogout } from '@/features/Authentication/services/Auth'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    user: null,
  },
  mutations: {
    setUser(state, user) {
      state.user = user;
    },
  },
  actions: {
    async register({ commit }, data) {
      commit('setUser', await requestRegistration(data));
    },
    async login({ commit }, credentials) {
      commit('setUser', await requestLogin(credentials));
    },
    async logout({ commit }) {
      try {
        await requestLogout();
      } catch(err) {
        console.log(err);
      }
      commit('setUser', null);
    },
    async reloadUser({ commit }) {
      try {
        commit('setUser', await getUser());
      } catch(err) {
        commit('setUser', null);
      }
    },
  },
  modules: {
  },
  getters: {
    signedIn: state => !!state.user,
  },
})
